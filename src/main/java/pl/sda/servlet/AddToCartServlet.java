package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@WebServlet(urlPatterns = "/addToCart")
public class AddToCartServlet extends HttpServlet {

    private static final long serialVersionUID = -8569559084769491721L;
    private ProductDb productDb;
    private Map<Product, Integer> cart;


    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest
                .getRequestDispatcher("/addToCart.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {

        final Long id = Long.valueOf(httpServletRequest.getParameter("productId"));
        final Integer quantity = Integer.valueOf(httpServletRequest.getParameter("quantity"));
        final Optional<Product> productById = this.productDb.getProductById(id);
        Cart cart = (Cart) httpServletRequest.getSession().getAttribute("cart");

        if (productById.isPresent()) {
            if (cart == null) {
                cart = new Cart();
            }
            cart.updateQuantity(productById.get(), quantity);
            httpServletRequest.getSession().setAttribute("cart", cart);
        }
        httpServletRequest.getRequestDispatcher("/addToCart.jsp").forward(httpServletRequest, httpServletResponse);
    }

    @Override
    public void init() throws ServletException {
        this.productDb = ProductDb.getSingletonInstance();
        super.init();
    }
}
