package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {

    private static final long serialVersionUID = -6444677165788599750L;
    private UserDb userDb;

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest
                .getRequestDispatcher("/register.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        final String login = httpServletRequest.getParameter("login");
        final String password = httpServletRequest.getParameter("password");
        final String email = httpServletRequest.getParameter("email");

        final User loggedUser = this.userDb.createNewUser(login, password, email);
        httpServletRequest.getSession().setAttribute("loggedUser", loggedUser);
        httpServletResponse.sendRedirect("/home");
    }

    @Override
    public void init() throws ServletException {
        this.userDb = UserDb.getSingletonInstance();
        super.init();
    }
}
