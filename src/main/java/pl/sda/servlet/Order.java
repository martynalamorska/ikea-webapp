package pl.sda.servlet;

import java.util.Map;

public class Order {

    Map<Product, Integer> order;
    Long price;
    BuyerData buyerData;

    public Order(final Map<Product, Integer> order, final Long price, final BuyerData buyerData) {
        this.order = order;
        this.price = price;
        this.buyerData = buyerData;
    }

    public Map<Product, Integer> getOrder() {
        return this.order;
    }

    public Long getPrice() {
        return this.price;
    }

    public BuyerData getBuyerData() {
        return this.buyerData;
    }
}
