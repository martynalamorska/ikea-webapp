package pl.sda.servlet;

public class BuyerData {

    String name;
    String streetAddress;
    String city;
    String phoneNo;

    public BuyerData(final String name, final String streetAddress, final String city, final String phoneNo) {
        this.name = name;
        this.streetAddress = streetAddress;
        this.city = city;
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return this.name;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public String getCity() {
        return this.city;
    }

    public String getPhoneNo() {
        return this.phoneNo;
    }


    BuyerData createNewBuyer(final String name, final String streetAddress, final String city, final String phoneNo) {
        final BuyerData buyerData = new BuyerData(name, streetAddress, city, phoneNo);
        return buyerData;
    }
}
