package pl.sda.servlet;

import java.util.Objects;

public class Product {

    private final long id;
    private final String name;
    private final String description;
    private final String category;
    private final long price;
    private final String href;

    public Product(final long id, final String name, final String description, final String category, final long price, final String href) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.price = price;
        this.href = href;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getCategory() {
        return this.category;
    }

    public long getPrice() {
        return this.price;
    }

    public String getHref() {
        return this.href;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final Product product = (Product) o;
        return this.id == product.id &&
                this.price == product.price &&
                this.name.equals(product.name) &&
                this.description.equals(product.description) &&
                this.category.equals(product.category) &&
                Objects.equals(this.href, product.href);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.name, this.description, this.category, this.price, this.href);
    }

    @Override
    public String toString() {
        return this.id + ". " + this.name + " (" + this.price + " PLN)";
    }
}
