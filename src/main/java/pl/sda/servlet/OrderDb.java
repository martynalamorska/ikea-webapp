package pl.sda.servlet;

import java.util.ArrayList;
import java.util.List;

public class OrderDb {

    private static OrderDb instance = null;
    private final List<Order> orders;

    public OrderDb() {
        this.orders = new ArrayList<>();
    }

    static OrderDb getSingletonInstance() {
        if (OrderDb.instance == null) {
            OrderDb.instance = new OrderDb();
        }
        return OrderDb.instance;
    }

    List<Order> getAllOrders() {
        return this.orders;
    }


}
