package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/viewProduct")
public class ViewProductServlet extends HttpServlet {
    private static final long serialVersionUID = 5494918379696208936L;
    private ProductDb productDb;

    @Override
    public void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        final long id = Long.valueOf(httpServletRequest.getParameter("id"));
        final Optional<Product> product = this.productDb.getProductById(id);

        httpServletRequest.setAttribute("newProduct", product);
        final Cookie lastView = new Cookie("lastView", String.valueOf(id));
        httpServletResponse.addCookie(lastView);
        httpServletRequest
                .getRequestDispatcher("/viewProduct.jsp")
                .forward(httpServletRequest, httpServletResponse);

    }

    @Override
    public void init() throws ServletException {
        this.productDb = ProductDb.getSingletonInstance();
        super.init();
    }
}
