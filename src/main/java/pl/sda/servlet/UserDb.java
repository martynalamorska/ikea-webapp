package pl.sda.servlet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UserDb {

    private static UserDb instance = null;
    private final List<User> users;

    private UserDb() {
        this.users = new ArrayList<>();

        this.users.add(new User("admin", "admin", "admin@admin.com", Arrays.asList(Roles.USER, Roles.ADMIN)));
    }

    static UserDb getSingletonInstance() {
        if (UserDb.instance == null) {
            UserDb.instance = new UserDb();
        }
        return UserDb.instance;
    }

    List<User> getAllUsers() {
        return this.users;
    }

    Optional<User> getUserByLogin(final String login) {
        return this.users
                .stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst();
    }

    Optional<User> getUserByEmail(final String email) {
        return this.users
                .stream()
                .filter(user -> user.getLogin().equals(email))
                .findFirst();
    }

    public User createNewUser(final String login, final String password, final String email) {

        final boolean emailDoesNotExist = this.users.stream().noneMatch(user -> user.getEmail().equals(email));
        final boolean loginDoesNotExist = this.users.stream().noneMatch(user -> user.getLogin().equals(login));

        if (emailDoesNotExist && loginDoesNotExist) {
            final User newUser = new User(login, password, email, Arrays.asList(Roles.USER));
            this.users.add(newUser);
            return newUser;
        } else {
            throw new FailedRegistrationException();
        }
    }

}
