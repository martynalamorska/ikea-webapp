package pl.sda.servlet;

public class FailedLoginException extends RuntimeException {
    private static final long serialVersionUID = -8142830692296237230L;

    public FailedLoginException(final String message) {
        super(message);
    }

}
