package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = -2947432406571084796L;
    private UserDb userDb;

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest
                .getRequestDispatcher("/login.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {

        final String login = httpServletRequest.getParameter("login");
        final String password = httpServletRequest.getParameter("password");
        final String result;
        final String loggedUser;
        if ((!password.isEmpty()) && (!login.isEmpty())) {
            final Optional<User> optUser = this.userDb.getAllUsers().stream()
                    .filter(user -> user.getLogin().equals(login))
                    .filter(user -> user.getPassword().equals(password))
                    .findFirst();
            if (optUser.isPresent()) {
                httpServletRequest.getSession().setAttribute("loggedUser", optUser.get());
                httpServletResponse.sendRedirect("/home");
            } else {
                result = "Wrong login or password";
                httpServletRequest.setAttribute("result", result);
                httpServletRequest.getRequestDispatcher("/login.jsp")
                        .forward(httpServletRequest, httpServletResponse);
            }
        } else {
            result = "Please fill in both fields";
            httpServletRequest.setAttribute("result", result);
            httpServletRequest.getRequestDispatcher("/login.jsp")
                    .forward(httpServletRequest, httpServletResponse);
        }
    }

    @Override
    public void init() throws ServletException {
        this.userDb = UserDb.getSingletonInstance();
        super.init();
    }
}
