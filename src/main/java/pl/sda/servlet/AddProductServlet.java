package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/addProduct", name = "addProduct")
public class AddProductServlet extends HttpServlet {

    private static final long serialVersionUID = -1368938975709755710L;
    private ProductDb productDb;

    @Override
    public void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {

        httpServletRequest
                .getRequestDispatcher("/addProduct.jsp")
                .forward(httpServletRequest, httpServletResponse);

    }

    @Override
    public void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {

        final String name = httpServletRequest.getParameter("name");
        final String description = httpServletRequest.getParameter("description");
        final String category = httpServletRequest.getParameter("category");
        final Long price = Long.valueOf(httpServletRequest.getParameter("price"));
        final String href = httpServletRequest.getParameter("href");

        final long newProduct = this.productDb.createNewProduct(name, description, category, price, href);

        httpServletResponse.sendRedirect("/viewProduct?id=" + newProduct);
    }

    @Override
    public void init() throws ServletException {
        this.productDb = ProductDb.getSingletonInstance();
        super.init();
    }
}
