package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/home")
public class MyFirstServlet extends HttpServlet {
    private static final long serialVersionUID = 8064378489100104072L;
    private ProductDb productDb;

    @Override
    public void doGet(final HttpServletRequest httpServletRequest,
                      final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {

        final String category = httpServletRequest.getParameter("category");
        List<Product> products = this.productDb.getAllProducts();
        if (category != null) {
            products = products.stream().filter(prod -> prod.getCategory().contains(category)).collect(Collectors.toList());
        }

        for (final Cookie cookie : httpServletRequest.getCookies()) {
            if (cookie.getName().equals("lastView")) {
                final long id = Long.valueOf(cookie.getValue());
                final Optional<Product> advertProduct = this.productDb.getProductById(id);
                advertProduct.ifPresent(product -> httpServletRequest.setAttribute("advert", (product)));
            }
        }

        httpServletRequest.setAttribute("products", products);

        httpServletRequest
                .getRequestDispatcher("/index.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    public void init() throws ServletException {
        this.productDb = ProductDb.getSingletonInstance();
        super.init();
    }
}
