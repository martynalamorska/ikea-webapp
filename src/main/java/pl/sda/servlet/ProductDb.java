package pl.sda.servlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class ProductDb {

    private static ProductDb instance = null;
    private final List<Product> products;

    private ProductDb() {
        this.products = new ArrayList<>();

        this.products.add(new Product(1, "Chair", "A comfy wooden chair", "Furniture", 99, "https://www.ikea.com/us/en/images/products/ekedalen-chair-brown__0516603_PE640439_S4.JPG"));
        this.products.add(new Product(2, "Table", "Small wooden table", "Furniture", 150, "https://www.ikea.com/PIAimages/0517381_PE640656_S5.JPG"));
        this.products.add(new Product(3, "Desk lamp", "A small lamp to place on a desk", "Lamps", 69, "https://pbs.twimg.com/profile_images/761330995270385664/TAGIGu2M_400x400.jpg"));
    }

    static ProductDb getSingletonInstance() {
        if (ProductDb.instance == null) {
            ProductDb.instance = new ProductDb();
        }
        return ProductDb.instance;
    }

    List<Product> getAllProducts() {
        return this.products;
    }

    Optional<Product> getProductById(final long id) {
        return this.products
                .stream()
                .filter(product -> product.getId() == id)
                .findFirst();
    }

    long createNewProduct(final String name, final String description, final String category, final Long price, final String href) {
        final long id = this.products.size();
        this.products.add(new Product(id + 1, name, description, category, price, href));

        return id + 1;
    }

}
