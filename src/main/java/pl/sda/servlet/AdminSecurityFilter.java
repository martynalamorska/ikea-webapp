package pl.sda.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "adminSecurityFilter", servletNames = {"addProduct"})
public class AdminSecurityFilter implements Filter {
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest servReq = (HttpServletRequest) servletRequest;
        final HttpServletResponse servRes = (HttpServletResponse) servletResponse;
        final User logUser = (User) servReq.getSession().getAttribute("loggedUser");
        if (logUser != null && logUser.getRoles().contains(Roles.ADMIN)) {
            filterChain.doFilter(servReq, servRes);
        } else {
            servRes.sendRedirect("/home");
        }
    }

    @Override
    public void destroy() {

    }
}
