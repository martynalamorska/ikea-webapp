package pl.sda.servlet;

public class FailedRegistrationException extends RuntimeException {
    private static final long serialVersionUID = 8877434895587102630L;

    public FailedRegistrationException() {
        super("Failed to register a new user");
    }
}
