package pl.sda.servlet;

import java.util.List;

public class User {

    String login;
    String password;
    String email;
    List<Roles> roles;
    BuyerData buyerData;

    public User(final String login, final String password, final String email, final List<Roles> roles) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.roles = roles;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public String getLogin() {
        return this.login;
    }

    public List<Roles> getRoles() {
        return this.roles;
    }

    public BuyerData getBuyerData() {
        return this.buyerData;
    }

    public User setBuyerData(final BuyerData buyerData) {
        this.buyerData = buyerData;
        return this;
    }

}
