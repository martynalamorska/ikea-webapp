package pl.sda.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/buyData")
public class BuyDataServlet extends HttpServlet {

    private static final long serialVersionUID = -2168609982962526854L;
    BuyerData buyerData;
    private UserDb userDb;

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest
                .getRequestDispatcher("/buyData.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        final String name = httpServletRequest.getParameter("name");
        final String streetAddress = httpServletRequest.getParameter("streetAddress");
        final String city = httpServletRequest.getParameter("city");
        final String phoneNo = httpServletRequest.getParameter("phoneNo");

        final BuyerData buyerData = this.buyerData.createNewBuyer(name, streetAddress, city, phoneNo);
        httpServletRequest.getSession().setAttribute("buyerData", buyerData);
        httpServletResponse.sendRedirect("/buy");
    }

    @Override
    public void init() throws ServletException {
        this.userDb = UserDb.getSingletonInstance();
        super.init();
    }
}
