package pl.sda.servlet;

import java.util.HashMap;
import java.util.Map;

public class Cart {

    private final Map<Product, Integer> products;

    public Cart() {
        this.products = new HashMap<>();
    }

    public Map<Product, Integer> get() {
        return new HashMap<>(this.products);
    }

    public void updateQuantity(final Product product, final Integer quantity) {
        //mniej czytelnie:
        final Integer oldQuantity = this.products.getOrDefault(product, 0);
        if (oldQuantity + quantity > 0) {
            this.products.put(product, oldQuantity + quantity);
        } else {
            this.products.remove(product);
        }
    }

//        if (this.products.containsKey(product)) {
//            final Integer oldQuantity = this.products.get(product);
//            if (oldQuantity + quantity > 0) {
//                this.products.put(product, oldQuantity + quantity);
//            } else {
//                this.products.remove(product);
//            }
//        } else if (quantity > 0) {
//            this.products.put(product, quantity);
//        }


}
