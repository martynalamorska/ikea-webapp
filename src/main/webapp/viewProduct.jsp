<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: marty
  Date: 06.04.2019
  Time: 11:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product View</title>
</head>
<body>
<jsp:include page="header.jsp"/>

<c:choose>
    <c:when test="${newProduct.isPresent()}">
        <div>
    <span>
            <h4> ${newProduct.get().getId()}. ${newProduct.get().getName()} </h4>
            <img src=${newProduct.get().getHref()} alt="Prod_picture" width="100" height="100">
            <p>Description: ${newProduct.get().getDescription()}</p>
            <p>Category: ${newProduct.get().getCategory()}</p>
            <p>Price (PLN): ${newProduct.get().getPrice()}</p>

    </span>
        </div>
    </c:when>
    <c:otherwise>
        Product doesn't exist.
    </c:otherwise>
</c:choose>


</body>
</html>
