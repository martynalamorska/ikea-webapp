<%--
  Created by IntelliJ IDEA.
  User: marty
  Date: 13.04.2019
  Time: 14:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/buyData" method="post">

    <p>Fill in your personal data
    <p/>

    <p>Name:
        <input type="text" name="name"/>
    </p>
    <p>Address (street and house number):
        <input type="text" name="streetAddress"/>
    </p>
    <p>City:
        <input type="text" name="city"/>
    </p>
    <p>Phone number:
        <input type="text" name="phoneNo"/>
    </p>
    <p>E-mail address:
        <input type="text" name="email"/>
    </p>
    <p>
        <input type="submit" name="submit" value="Proceed to finalize your order"/>
    </p>
</form>

</body>
</html>
