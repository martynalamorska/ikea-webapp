<%@ page import="pl.sda.servlet.Cart" %>
<%@ page import="pl.sda.servlet.Product" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: marty
  Date: 13.04.2019
  Time: 09:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cart</title>
</head>
<body>
<jsp:include page="header.jsp"/>

<h2>Cart</h2>
<div>

    <%
        final Cart cart = (Cart) session.getAttribute("cart");
        if (cart != null) {
            for (final Map.Entry<Product, Integer> entry : cart.get().entrySet()) {
    %>
    <%= entry.getKey().getId() + ". " + entry.getKey().getName() + " (" + entry.getKey().getPrice() + " PLN)"%>
    <%= " x " + entry.getValue()%>
    <% out.println(); %>
    <form action="${pageContext.request.contextPath}/addToCart" method="post">
        <input type="number" name="quantity"/>
        <input type="hidden" value="<%=entry.getKey().getId()%>" name="productId"/>
        <input type="submit" name="submit" value="Add/remove products"/>
    </form>

    <%
        }%>
    <form action=${pageContext.request.contextPath}/buyData" method="get">
        <input type="submit" name="submit" value="Buy"/>
    </form>
    <%
        } else {
            out.println("Your cart is empty.");
        }
    %>
</div>
<div>


</div>

</body>
</html>
