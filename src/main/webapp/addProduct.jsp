<%--
  Created by IntelliJ IDEA.
  User: Martyna
  Date: 06.04.2019
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Product</title>
</head>
<body>
<jsp:include page="header.jsp"/>

<form action="${pageContext.request.contextPath}/addProduct" method="post">

    <p>Add a new product:
    <p/>

    <p> Name:
        <input type="text" name="name"/>
    </p>
    <p>Description:
        <input type="text" name="description"/>
    </p>
    <p>Category:
        <input type="text" name="category"/>
    </p>
    <p>Price:
        <input type="text" name="price"/>
    </p>
    <p>Photo:
        <input type="text" name="href"/>
    </p>
    <p>
        <input type="submit" name="submit" value="Add"/>
    </p>
</form>


</body>
</html>
