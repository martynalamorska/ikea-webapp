<%@ page import="pl.sda.servlet.Roles" %>
<%@ page import="pl.sda.servlet.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<jsp:include page="header.jsp"/>


<div align="right">
    <h4> ${advert.getId()}. ${advert.getName()}</h4>
    <img src=${advert.getHref()} alt="Prod_picture" width="50" height="50">
    <p>Price (PLN): ${advert.getPrice()}</p>
</div>

<%
    final User logUs = (User) session.getAttribute("loggedUser");
    if (logUs != null && logUs.getRoles().contains(Roles.ADMIN)) {
%>
<form action="${pageContext.request.contextPath}/addProduct" method="get">
    <p>Add a new product
        <input type="submit" name="submit" value="Add"/>
    </p>
</form>
<% } else {

}

%>


<form action="${pageContext.request.contextPath}/home" method="get">
    <p>Filter the products by category
        <input type="text" name="category"/>
        <input type="submit" name="submit" value="Filter"/>
    </p>
</form>


<c:forEach items="${products}" var="prod">
    <div>
    <span>
            <a href="${pageContext.request.contextPath}/viewProduct?id=${prod.getId()}">
                <h4> ${prod.getId()}. ${prod.getName()} </h4></a>
        <img src=${prod.getHref()} alt="Prod_picture" width="100" height="100">
            <p>Description: ${prod.getDescription()}</p>
            <p>Category: ${prod.getCategory()}</p>
            <p>Price (PLN): ${prod.getPrice()}</p>

        <form action="${pageContext.request.contextPath}/addToCart" method="post">
            <input type="number" name="quantity" pattern="^[0-999999999]" min="1" step="1"/>
            <input type="hidden" value="${prod.getId()}" name="productId"/>
            <input type="submit" name="submit" value="Add to cart"/>
        </form>
        <br><br>

    </span>
    </div>


</c:forEach>


<p>${product}</p>
<jsp:include page="footer.jsp"/>
</body>
</html>
