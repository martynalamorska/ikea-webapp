<%@ page import="pl.sda.servlet.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Martyna
  Date: 06/04/2019
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>IKEA 2.0</title>
</head>
<body>

<%
    final User loggedUser = (User) session.getAttribute("loggedUser");
    if (loggedUser != null) {
%>
<form action="${pageContext.request.contextPath}/logout" method="get">
    <p align="right">
        <% out.println(" Hello " + loggedUser.getLogin() + "    "); %>
        <%--                Hello ${loggedUser.getLogin()}--%>
        <input type="submit" name="submit" value="Sign out"/>
    </p>
</form>

<% } else { %>
<form action="${pageContext.request.contextPath}/login" method="get">
    <p align="right">
        <input type="submit" name="submit" value="Sign in"/>
    </p>
</form>
<% } %>


<h1> IKEA 2.0 </h1>

<%--    <%--%>
<%--        final User loggedUser = (User) session.getAttribute("loggedUser");--%>
<%--        if (loggedUser != null) {--%>
<%--    %> <form action="${pageContext.request.contextPath}/logout" method="get">--%>
<%--        <p align="right">--%>
<%--            <% out.println(" HELLO " + loggedUser.getLogin().toUpperCase()); %>--%>
<%--&lt;%&ndash;                Hello ${loggedUser.getLogin()}&ndash;%&gt;--%>
<%--                <input type="submit" name="submit" value="Sign out"/>--%>
<%--            </p>--%>
<%--        </form>--%>

<%--    <% } else { %>--%>
<%--        <form action="${pageContext.request.contextPath}/login" method="get">--%>
<%--            <p align="right">--%>
<%--                <input type="submit" name="submit" value="Sign in"/>--%>
<%--            </p>--%>
<%--        </form>--%>
<%--    <% } %>--%>


<%--<c:choose>--%>
<%--    <c:when test="${not empty loggedUser}">--%>
<%--        <form action="${pageContext.request.contextPath}/logout" method="get">--%>
<%--            <p align="right">--%>
<%--                Hello ${loggedUser.getLogin()}--%>
<%--                <p align="right">--%>
<%--                <input type="submit" name="submit" value="Sign out"/>--%>
<%--                </p>--%>
<%--            </p>--%>
<%--        </form>--%>
<%--    </c:when>--%>
<%--    <c:otherwise>--%>
<%--        <form action="${pageContext.request.contextPath}/login" method="get">--%>
<%--            <p align="right">--%>
<%--                <input type="submit" name="submit" value="Sign in"/>--%>
<%--            </p>--%>
<%--        </form>--%>
<%--    </c:otherwise>--%>

<%--</c:choose>--%>


<menu>
    <h2>Menu</h2>
    <ul>
        <li><a href="${pageContext.request.contextPath}/home">Home</a></li>
        <li><a href="${pageContext.request.contextPath}/register">Register</a></li>
        <li><a href="${pageContext.request.contextPath}/addToCart">Cart</a></li>
        <%
            final User logUs = (User) session.getAttribute("loggedUser");
            if (logUs != null) {
        %>
        <li><a href="${pageContext.request.contextPath}/orders">Orders</a></li>
        <% } else {
        }
        %>
        <li><a href="${pageContext.request.contextPath}/home">About</a></li>
    </ul>
</menu>


</body>
</html>
